* This project is an (unfinished) attempt to reimplement UML 1.0 
diagrams from Thomas Pender's book "UML Weekend Crash Course"
in PlantUML.

I don't intend to continue it much, because I found that although 
PlantUML's language is quite powerful, it doesn't implement all of the UML,
and what is more annoying, doesn't allow to neither combine elements
from different diagrams on one sheet nor explicitly state the type 
of a diagram. (So sometimes PlantUML starts to misinterpret the 
notation.)

On the other hand, I still believe that UML might make sense, and that
the textual form is the only viable representation of a programming
language. So maybe, in some time I will get back to this project.
(Or maybe not.)

The resulting published task page:

[Click here to see the document.](tasks-and-diagrams.md "Direct link.")



