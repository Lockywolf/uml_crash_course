
# Table of Contents

1.  [Uml and Systems Engineering](#orgf882921)
    1.  [Pender's book](#org6fa2a51)
        1.  [Diagram rewriting](#org27fc8fa)
2.  [SysML](#org5f49432)

In this file I want to write all my tasks, as I have planned them.
This file is expected to be a continuation of my paper planner.
I will still be using the paper planner for daily activities, but there seems to be just too many tasks.


<a id="orgf882921"></a>

# DONE Uml and Systems Engineering


<a id="org6fa2a51"></a>

## Pender's book


<a id="org27fc8fa"></a>

### Diagram rewriting

1.  Figure 1-1 Packages of the UML Metamodel

    ![img](figure1-1.png)
    
    1.  TODO <https://forum.plantuml.net/9794/create-diagram-question-packages-headers-labelled-bodies>

2.  Figure 1-2 The contents of the Foundation package

    ![img](figure1-2.png)

3.  Figure 1-3 A stereotype on a class

    ![img](figure1-3.png)

4.  Figure 1-4 Comment notation

    ![img](figure1-4.png)

5.  Figure 3-1 Three complimentary views or sets of diagrams

    1.  TODO Cannot be implemented in UML

6.  Figure 3-2 Elements of the Functional View

    1.  TODO Cannot be implemented in UML

7.  Figure 3-3 Class diagram (top) and Object diagram (bottom)

    ![img](figure3-3.png)
    
    1.  TODO Multipage diagrams don't work. I asked org-mode people, but need to track.

8.  TODO Figure 3-4 Sequence, Collaboration and Statechard diagrams. (Not working)

    ![img](figure3-4.png)
    
    ![img](figure3-4-2.png)
    
    ![img](figure3-4-3.png)
    
    ![img](figure3-5.png)

9.  TODO Figure 5-1

    1.  TODO Figure 5-1-1 (Incomplete)
    
        ![img](figure5-1-1.png)
    
    2.  TODO Figure 5-1-2 (Incomplete)
    
        <https://forum.plantuml.net/9864/salt-diagrams-ignore-the-caption-keywork-and-packages>
        
        ![img](figure5-1-2.png)
    
    3.  TODO Figure 5-1-3 (Incomplete)
    
        <https://forum.plantuml.net/9865/cannot-put-an-activity-diagram-into-a-package>
        
        ![img](figure5-1-3.png)

10. TODO Figure 5-3 Elements of a Use Case Diagram

    ![img](figure5-3.png)

11. TODO Figure 5-4 System icon for the Use Case diagram

    ![img](figure5-4-1.png)
    
    ![img](figure5-4-2.png)

12. TODO Figure 5-5 Actor icons for the Use Case diagram

    ![img](figure5-5.png)

13. TODO Figure 5-6 Use Case notation for the Use Case diagram

    ![img](file5-6.png)

14. TODO Figure 5-7 Association notation for the Use Case diagram

    ![img](figure5-7.png)

15. DONE Figure 5-8 <a id="org80358a7"></a> dependency notation for the Use Case diagram

    ![img](figure5-8.png)

16. TODO Figure 5-9 <a id="orgf6b7a4f"></a> dependency notation for the Use Case diagram

    ![img](figure5-9.png)

17. TODO Figure 5-10 Generalization notation for the Use Case diagram

    ![img](figure5-10.png)

18. DONE Figure 6-1 System-type actors from the problem statement

    ![img](figure6-1.png)

19. \_<sub>Now</sub> I will stop making footers, it is becoming repetitive\_\_

20. TODO Figure 6-2 Human actors from the problem statement

    ![img](figure6-2.png)

21. Pender speaks of a use case "narrative method". (Section 7.)

    The idea is generally just a few recommendations on making templates
    for working documents.
    Still, storage of those should be in BitBucket or Confluence.

22. Figure 8-1 Activity diagram for Fill Order Use Case

    ![img](figure8-1.png)
    
    ![img](figure8-1old.png)
    
    ![img](test.png)


<a id="org5f49432"></a>

# TODO SysML

Then I wanted to switch to the SysML part &#x2013; that is, have a look at 
how PlantUML implements SysML.

